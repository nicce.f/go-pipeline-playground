package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", Hello)

	log.Fatal(http.ListenAndServe(":8080", nil))
}

// Hello : return hello message
func Hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, %q", string(r.URL.Query()["name"][0]))
}
